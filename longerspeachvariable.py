#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 11 15:42:16 2019

@author: kiran
"""
import time 
import azure.cognitiveservices.speech as speechsdk
speech_key, service_region = "20b2c81f345d4483a65d727b6e4bbc94", "westus"
speech_config = speechsdk.SpeechConfig(subscription=speech_key, region=service_region, speech_recognition_language='en-US')
audiofilename = ("/home/kiran/shyne_nlp/assingment/code/speachToText/3371e3ad0930c22716962a6b15910142.wav")
speech_recognizer = speechsdk.SpeechRecognizer(speech_config=speech_config)
def speech_recognize_continuous_from_file():
    """performs continuous speech recognition with input from an audio file"""
    speech_config = speechsdk.SpeechConfig(subscription=speech_key, region=service_region,speech_recognition_language='en-US')
    speech_config.output_format=speechsdk.OutputFormat.Detailed
    audio_config = speechsdk.audio.AudioConfig(filename=audiofilename)
 
    speech_recognizer = speechsdk.SpeechRecognizer(speech_config=speech_config, audio_config=audio_config)
 
    done = False
 
    def stop_cb(evt):
        """callback that stops continuous recognition upon receiving an event `evt`"""
        #print('CLOSING on {}'.format(evt.result.text))
        speech_recognizer.stop_continuous_recognition()
        nonlocal done
        done = True
 
    # Connect callbacks to the events fired by the speech recognizer
    #speech_recognizer.recognizing.connect(lambda evt: print('RECOGNIZING: {}'.format(evt)))
    x = []
    #speech_recognizer.recognized.connect(lambda evt: print(evt.result.json))
    speech_recognizer.recognized.connect(lambda evt: x.append(evt.result.json))
    #speech_recognizer.recognized.connect(lambda evt: x.append(evt.result.text))
   
    speech_recognizer.session_stopped.connect(stop_cb)
    speech_recognizer.canceled.connect(stop_cb)
 
    # Start continuous speech recognition
    speech_recognizer.start_continuous_recognition()
    while not done:
        time.sleep(.5)
        
    print("x text here")
    print(x)
        
    return(x)
        
        
if __name__ == '__main__':
    speech_recognize_continuous_from_file()
