#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 11 15:42:16 2019

@author: kiran
"""
import time 
import azure.cognitiveservices.speech as speechsdk
speech_key, service_region = "20b2c81f345d4483a65d727b6e4bbc94", "westus"
speech_config = speechsdk.SpeechConfig(subscription=speech_key, region=service_region, speech_recognition_language='en-US')
audiofilename = ("/home/kiran/shyne_nlp/assingment/code/speachToText/Call99606809689960680968_201.wav")
speech_recognizer = speechsdk.SpeechRecognizer(speech_config=speech_config)
def speech_recognize_continuous_from_file():
    """performs continuous speech recognition with input from an audio file"""
    speech_config = speechsdk.SpeechConfig(subscription=speech_key, region=service_region,speech_recognition_language='en-US')
    audio_config = speechsdk.audio.AudioConfig(filename=audiofilename)
 
    speech_recognizer = speechsdk.SpeechRecognizer(speech_config=speech_config, audio_config=audio_config)
 
    done = False
 
    def stop_cb(evt):
        """callback that stops continuous recognition upon receiving an event `evt`"""
        print('CLOSING on {}'.format(evt))
        speech_recognizer.stop_continuous_recognition()
        nonlocal done
        done = True
 
    # Connect callbacks to the events fired by the speech recognizer
    speech_recognizer.recognizing.connect(lambda evt: print('RECOGNIZING: {}'.format(evt)))
    speech_recognizer.recognized.connect(lambda evt: print('RECOGNIZED: {}'.format(evt)))
    speech_recognizer.session_started.connect(lambda evt: print('SESSION STARTED: {}'.format(evt)))
    speech_recognizer.session_stopped.connect(lambda evt: print('SESSION STOPPED {}'.format(evt)))
    speech_recognizer.canceled.connect(lambda evt: print('CANCELED {}'.format(evt)))
    # stop continuous recognition on either session stopped or canceled events
    speech_recognizer.session_stopped.connect(stop_cb)
    speech_recognizer.canceled.connect(stop_cb)
 
    # Start continuous speech recognition
    speech_recognizer.start_continuous_recognition()
    while not done:
        time.sleep(.5)
        
        
if __name__ == '__main__':
    speech_recognize_continuous_from_file()
